(ns hello-ci.core-test
  (:require [midje.sweet :refer :all]
            [hello-ci.core :refer :all]))

(fact "1 + 1 should be 2"
      (math-fn 1 1) => 2)

(fact "2 + 3 shouldn't be 6"
      (math-fn 2 3) not=> 6)
